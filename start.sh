#!/usr/bin/env bash

set -e

# ES_CA_CERT is passed as the full-value, but fluent-bit wants a filename
if [[ -z "${ES_CA_CERT}" ]]; then
  rm -f /fluent-bit/etc/ca.crt
else
  echo "${ES_CA_CERT}" > /fluent-bit/etc/ca.crt
  ES_CA_FILE="/fluent-bit/etc/ca.crt"
  export ES_CA_FILE
fi

bin/fluent-bit -c etc/fluent-bit.conf

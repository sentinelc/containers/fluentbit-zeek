-- zeek ts field is a float, but fluentbit wants a string.
-- https://github.com/fluent/fluent-bit/issues/1647
function time2str(tag, timestamp, record)
         return 1, record["ts"], record
end

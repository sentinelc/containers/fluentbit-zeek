require('parse_helpers')

-- zeerbit-zeek-scripts/conn-add-geo
function zeek_conn_parse_geo(tag, timestamp, record)
  -- adds the following fields for final nesting under source.geo and destination.geo
  -- source_geo.location.lon
  -- source_geo.location.lat
  -- source_geo.country_iso_code
  -- source_geo.city_name
  -- source_geo.region_name
  -- destination_geo.location.lon
  -- destination_geo.location.lat
  -- destination_geo.country_iso_code
  -- destination_geo.city_name
  -- destination_geo.region_name

  local source_geo_lon         = record["zeek_connection_geo.orig.longitude"]
  local source_geo_lat         = record["zeek_connection_geo.orig.latitude"]
  local source_geo_cc          = record["zeek_connection_geo.orig.country_code"]
  local source_geo_region      = record["zeek_connection_geo.orig.region"]
  local source_geo_city        = record["zeek_connection_geo.orig.city"]
  local destination_geo_lon    = record["zeek_connection_geo.resp.longitude"]
  local destination_geo_lat    = record["zeek_connection_geo.resp.latitude"]
  local destination_geo_cc     = record["zeek_connection_geo.resp.country_code"]
  local destination_geo_region = record["zeek_connection_geo.resp.region"]
  local destination_geo_city   = record["zeek_connection_geo.resp.city"]

  local source_location = {}
  local destination_location = {}

  if source_geo_lon ~= nil and source_geo_lat ~= nil then
    source_location["lon"] = source_geo_lon
    source_location["lat"] = source_geo_lat
  end

  if destination_geo_lon ~= nil and destination_geo_lat ~= nil then
    destination_location["lon"] = destination_geo_lon
    destination_location["lat"] = destination_geo_lat
  end

  local modified = false
  if table_size(source_location) > 0 then
    record["source_geo"] = {}
    record["source_geo"]["location"]      = source_location
    modified = true
  end
  if table_size(destination_location) > 0 then
    record["destination_geo"] = {}
    record["destination_geo"]["location"] = destination_location
    modified = true
  end
  if type(source_geo_cc) == "string" and string.len(source_geo_cc) > 1 then
    record["source_geo"]["country_iso_code"] = source_geo_cc
    modified = true
  end
  if type(destination_geo_cc) == "string" and string.len(destination_geo_cc) > 1 then
    record["destination_geo"]["country_iso_code"] = destination_geo_cc
    modified = true
  end
  if type(source_geo_region) == "string" and string.len(source_geo_region) > 1 then
    record["source_geo"]["region_name"] = source_geo_region
    modified = true
  end
  if type(destination_geo_region) == "string" and string.len(destination_geo_region) > 1 then
    record["destination_geo"]["region_name"] = destination_geo_region
    modified = true
  end
  if type(source_geo_city) == "string" and string.len(source_geo_city) > 1 then
    record["source_geo"]["city_name"] = source_geo_city
    modified = true
  end
  if type(destination_geo_city) == "string" and string.len(destination_geo_city) > 1 then
    record["destination_geo"]["city_name"] = destination_geo_city
    modified = true
  end

  if modified then
    return 1, timestamp, record
  else
    return 0, timestamp, record
  end
end
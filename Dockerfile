FROM docker.io/fluent/fluent-bit:2.2.0-debug

RUN rm /fluent-bit/etc/*
COPY fluent-bit.conf /fluent-bit/etc/fluent-bit.conf
COPY zeek/ /fluent-bit/etc/zeek/
COPY parse_helpers.lua /fluent-bit
COPY --chmod=755 start.sh /fluent-bit

WORKDIR /fluent-bit
CMD ["/fluent-bit/start.sh"]
